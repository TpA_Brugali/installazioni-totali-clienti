
;--------------------------------

; Costanti

!define PRODUCT_APPLICATION_NAME "Cecilio Machines CD"
!define WELCOME_NAME             "Cecilio Machines"
!define PRODUCT_NAME             "MACHINE_CD"
!define FULL_PRODUCT_VERSION     "1.0.0.0"  ;Per pannello di controllo (vuole x.x.x.x)
!define PRODUCT_VERSION          "1.0"
!define PRODUCT_REVISION         "1"
!define PRODUCT_FOLDERNAME       "TPA"

;Installazioni di altri applicativi da aggiungere in modalit� silent
!define WSCM                     "Setup WSCM 3.6.exe"
!define WSCM_UNISTALL            "uninstWSCM.exe"
!define WSCM_SP                  "ServicePack_3.6_12.exe"
!define ALBATROS_SP_CLIPPER      "Albatros\Service Pack 4a ver3.1 Clipper.exe"
!define TPACAD_SP                "TpaCad\Setup Service Pack 11 ver1.3 TpaCAD.exe"
!define NESTING                  "Nesting\Nesting 3.4 Setup.exe"
!define NESTING_UNISTALL         "UninstallNesting.exe"
;!define NESTING_SP               "Nesting\ServicePack_3.2_2.exe"
!define ELABORATOR               "Elaborator\Setup Elaborator Ver1.1.exe"
!define ELABORATOR_UNISTALL      "Uninstall Elaborator WCStd.exe"
;!define ELABORATOR_SP            "Elaborator\Elaborator 1.1 Sp1.exe"


;------------------Parametrizzare in funzione del custom da installare------------------------
!define ALB_CUSTOM_0             "Custom\Cecilio_Mech.exe"
!define ALB_CUSTOM_1             "Custom\Cecilio_Analog.exe"
!define ALB_CUSTOM_2             ""

!define FIRST_CHOICE             "Snova - Mechatrolink"
!define SECOND_CHOICE            "Snova - Analogic"
!define THIRD_CHOICE             ""
;----------------------------------------------------------------------------------------------

!define BUILDERAPP               "BuilderApp\BuilderApp.exe"

!define SORGENTI                 "F:\WSC 4.0\Setup\Setup All\Cecilio\1.0 V1\Installation"

!define MUI_ABORTWARNING
!define MUI_COMPONENTSPAGE_SMALLDESC

;--------------------------------

; Variabili

Var StartMenuFolder    ; Cartella di menu
Var DirToCheck         ; Directory da controllare se vuota
Var DirState           ; Contiene l'informazione se directory vuota

;-------------------------------------------------------------------------------------------

; Decido l'algoritmo di compressione dell'eseguibile finale, da utilizzare sempre
; come prima istruzione
SetCompressor zlib

; Utilit� di base file e directory
!include FILEFUNC.NSH
; Utilit� di gestione sezioni
!include Sections.nsh
;Utilit� per gestione processi
!include nsProcess.nsh

;--------------------------------

; User interface definition

; MUI 2.0 compatible
!include MUI2.nsh

;Icona
!define MUI_ICON   "${SORGENTI}\Icons\multiSizeGeneric.ico"
!define MUI_UNICON "${SORGENTI}\Icons\multiSizeGenericUnist.ico"

;replace the "Nullsoft Install System v2.45"
BrandingText "T.P.A. S.p.A. Install"

;Pagine visualizzate nell'installazione
!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_LICENSE $(LicenseFile)  ; License page

;!define MUI_STARTMENUPAGE_DEFAULTFOLDER "TPA"
;!define MUI_STARTMENUPAGE_REGISTRY_ROOT "HKLM"
;!define MUI_STARTMENUPAGE_REGISTRY_KEY "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_NAME}_${PRODUCT_VERSION}.${PRODUCT_REVISION}"
;!define MUI_STARTMENUPAGE_REGISTRY_VALUENAME "NSIS:StartMenuDir"
;!define MUI_STARTMENUPAGE_NODISABLE
;!insertmacro MUI_PAGE_STARTMENU Application $StartMenuFolder

; Component pages
!insertmacro MUI_PAGE_COMPONENTS
; Directory page
!insertmacro MUI_PAGE_DIRECTORY
; Instfiles page
!insertmacro MUI_PAGE_INSTFILES
; Finish page
!insertmacro MUI_PAGE_FINISH

;Uninstall page
!insertmacro MUI_UNPAGE_INSTFILES

; Language files
!insertmacro MUI_LANGUAGE "Italian"
!insertmacro MUI_LANGUAGE "English"
!insertmacro MUI_LANGUAGE "French"
!insertmacro MUI_LANGUAGE "German"
!insertmacro MUI_LANGUAGE "Spanish"

;Messaggi in lingua
!include Messaggi.nsh

;--------------------------------

; Propriet�

Name                                  "${WELCOME_NAME}"
BrandingText                          "T.P.A. S.p.A. Install" ;replace the "Nullsoft Install System v2.45"
OutFile                               "${SORGENTI}\Setup ${WELCOME_NAME} ${PRODUCT_VERSION} V${PRODUCT_REVISION}.exe"
InstallDir                            "C:\Albatros\"
ShowInstDetails                       show
ShowUnInstDetails                     show

;--------------------------------

; Informazioni di versione

VIProductVersion "${FULL_PRODUCT_VERSION}"
VIAddVersionKey /LANG=${LANG_ENGLISH} "ProductName" "${PRODUCT_APPLICATION_NAME}"
VIAddVersionKey /LANG=${LANG_ENGLISH} "Comments" ""
VIAddVersionKey /LANG=${LANG_ENGLISH} "CompanyName" "T.P.A. S.p.A."
VIAddVersionKey /LANG=${LANG_ENGLISH} "LegalTrademarks" ""
VIAddVersionKey /LANG=${LANG_ENGLISH} "LegalCopyright" "(c) 2014 TPA Spa. All rights reserved."
VIAddVersionKey /LANG=${LANG_ENGLISH} "FileDescription" "${PRODUCT_APPLICATION_NAME} Setup"
VIAddVersionKey /LANG=${LANG_ENGLISH} "ProductVersion" "${PRODUCT_VERSION}"
VIAddVersionKey /LANG=${LANG_ENGLISH} "FileVersion" "1.0"

;--------------------------------

; Sections

Section /o "${FIRST_CHOICE}" First

  ;--------------------------------

  ; Copio i pacchetti d'installazione che mi servono

  CreateDirectory $INSTDIR
  CreateDirectory $INSTDIR\CD_Install
  CreateDirectory $INSTDIR\CD_Install\Albatros
  CreateDirectory $INSTDIR\CD_Install\TpaCad
  CreateDirectory $INSTDIR\CD_Install\Nesting
  CreateDirectory $INSTDIR\CD_Install\Elaborator
  CreateDirectory $INSTDIR\CD_Install\Custom
  CreateDirectory $INSTDIR\CD_Install\BuilderApp

  ;Applicazioni e Service Pack da installare - copiate in locale
  SetOverwrite ifnewer

  SetOutPath   "$INSTDIR\CD_Install"
  File         "${SORGENTI}\${WSCM}"
  File         "${SORGENTI}\${WSCM_SP}"

  SetOutPath  "$INSTDIR\CD_Install\Albatros"
  File        "${SORGENTI}\${ALBATROS_SP_CLIPPER}"

  SetOutPath   "$INSTDIR\CD_Install\TpaCad"
  File         "${SORGENTI}\${TPACAD_SP}"

  SetOutPath   "$INSTDIR\CD_Install\Nesting"
  File         "${SORGENTI}\${NESTING}"

  SetOutPath   "$INSTDIR\CD_Install\Elaborator"
  File         "${SORGENTI}\${ELABORATOR}"
  ;File        "${SORGENTI}${ELABORATOR_SP}"

  SetOutPath   "$INSTDIR\CD_Install\Custom"
  File         "${SORGENTI}\${ALB_CUSTOM_0}"

  SetOutPath   "$INSTDIR\CD_Install\BuilderApp"
  File         "${SORGENTI}\${BUILDERAPP}"

  ;--------------------------------

  ; Lancio tutti gli applicativi uno per uno, in modo da installarli tutti

  ;WSCM
  SetOutPath   "$INSTDIR"
  IfFileExists "$INSTDIR\CD_Install\${WSCM}" 0 +2
    ExecWait '"$INSTDIR\CD_Install\${WSCM}" /S /Path="$INSTDIR" /Version=Clipper /Simul3d=0 /Alb31=1 /Editor=TpaCad' $0

  SetOutPath   "$INSTDIR"
  IfFileExists "$INSTDIR\CD_Install\${WSCM_SP}" 0 +2
    ExecWait '"$INSTDIR\CD_Install\${WSCM_SP}" Path="$INSTDIR" -s Overwrite=1' $0              ;Cartelle BIN - LNG - TMP
    ;Devo attendere la fine dell'installazione del SP
    Call WaitSPUpdater

  ;TpaCad Sp (silent - Il file deve gi� trovarsi nella directory di installazione)
  SetOutPath   "$INSTDIR"
  IfFileExists "$INSTDIR\CD_Install\${TPACAD_SP}" 0 +2
    ExecWait '"$INSTDIR\CD_Install\${TPACAD_SP}" /S' $0                                        ;Cartelle BIN - DXF - HELP - LNG - TPACADCFG, file sovrascitti = Bin\Tpaspa.SplashAbaut.v2.dll

  ;Elaborator (silent - Il file deve gi� trovarsi nella directory di installazione)
  SetOutPath   "$INSTDIR"
  IfFileExists "$INSTDIR\CD_Install\${ELABORATOR}" 0 +2
    ExecWait '"$INSTDIR\CD_Install\${ELABORATOR}" /S' $0                                       ;Cartelle BIN, sovrascitti = MaacTecno.dll

;  ;Elaborator Sp (silent - Il file deve gi� trovarsi nella directory di installazione)
;  IfFileExists "$INSTDIR\CD_Install\${ELABORATOR_SP}" 0 +2
;    ExecWait '"$INSTDIR\CD_Install\${ELABORATOR_SP}" /S' $0

  ;BuilderApp (silent - Il file deve gi� trovarsi nella directory di installazione)
  SetOutPath   "$INSTDIR"
  IfFileExists "$INSTDIR\CD_Install\${BUILDERAPP}" 0 +2
    ExecWait '"$INSTDIR\CD_Install\${BUILDERAPP}" /S' $0                                       ;Cartelle BIN - LNG - TPACADCFG, nessuna sovrascittura

  ;Nesting, per ora lasciamolo perdere
  SetOutPath   "$INSTDIR"
  IfFileExists "$INSTDIR\CD_Install\${NESTING}" 0 +2
    ExecWait '"$INSTDIR\CD_Install\${NESTING}" /S /Path="$INSTDIR" /Type=APP' $0               ;Cartelle BIN - LNG - HELP - SYSTEM, file sovrascitti = Bin\AlnPiece.dll
                                                                                                                                                      ;Bin\Interop.ALBPIECEOPLib.dll
                                                                                                                                                      ;Bin\TpaSpa.CadGeos.v2.dll
                                                                                                                                                      ;Bin\TpaSpa.CadLibrary.v2.dll
                                                                                                                                                      ;Bin\TpaSpa.CadModel.v2.dll
                                                                                                                                                      ;Bin\TpaSpa.CadOpti.v2.dll
                                                                                                                                                      ;Bin\TpaSpa.CadPanelDraw.v2.dll
                                                                                                                                                      ;Bin\TpaSpa.CadPiece.v2.dll
                                                                                                                                                      ;Bin\TpaSpa.CadPreview.v2.dll
                                                                                                                                                      ;Bin\TpaSpa.CadTecno.v2.dll
                                                                                                                                                      ;Bin\TpaSpa.FieldSelector.v2.dll
                                                                                                                                                      ;Bin\TpaSpa.Share.v2.dll
                                                                                                                                                      ;Bin\TpaSpa.SplashAbout.v2.dll
                                                                                                                                                      ;Bin\TpaSpa.StandardOptim.v2.dll
                                                                                                                                                      ;Bin\TpaSpa.TpaMath.v2.dll


  ;Albatros Sp (silent - Il file deve gi� trovarsi nella directory di installazione)
  SetOutPath   "$INSTDIR"
  IfFileExists "$INSTDIR\CD_Install\${ALBATROS_SP_CLIPPER}" 0 +2
    ExecWait '"$INSTDIR\CD_Install\${ALBATROS_SP_CLIPPER}" Path="$INSTDIR" -s Overwrite=1' $0  ;Cartelle BIN - LNG - HELP - SYSTEM, file sovrascitti = Bin\TpaSpa.Language.v2.dll
                                                                                                                                                      ;Bin\TpaSpa.Share.v2.dll
                                                                                                                                                      ;Bin\TpaSpa.SplashAbout.v2.dll
                                                                                                                                                      ;Bin\TpaSpa.TpaIni.v2.dll
                                                                                                                                                      ;Bin\TpaSpa.TpaLanguage.v2.dll
                                                                                                                                                      
  ;Custom     (silent - Il file deve gi� trovarsi nella directory di installazione) gli attributi per il silent sono gi� presenti nell'archivio
  SetOutPath   "$INSTDIR"
  IfFileExists "$INSTDIR\CD_Install\${ALB_CUSTOM_0}" 0 +2
    ExecWait '"$INSTDIR\CD_Install\${ALB_CUSTOM_0}"' $0                                          ;Cartelle MOD0 - SYSTEM
    
  ;--------------------------------
  ; Inserirsco le chiavi di registro

  Call CommonRegistryKey

SectionEnd

Section /o "${SECOND_CHOICE}" Second

  ;--------------------------------

  ; Copio i pacchetti d'installazione che mi servono

  CreateDirectory $INSTDIR
  CreateDirectory $INSTDIR\CD_Install
  CreateDirectory $INSTDIR\CD_Install\Albatros
  CreateDirectory $INSTDIR\CD_Install\TpaCad
  CreateDirectory $INSTDIR\CD_Install\Nesting
  CreateDirectory $INSTDIR\CD_Install\Elaborator
  CreateDirectory $INSTDIR\CD_Install\Custom
  CreateDirectory $INSTDIR\CD_Install\BuilderApp

  ;Applicazioni e Service Pack da installare - copiate in locale
  SetOverwrite ifnewer

  SetOutPath   "$INSTDIR\CD_Install"
  File         "${SORGENTI}\${WSCM}"
  File         "${SORGENTI}\${WSCM_SP}"

  SetOutPath  "$INSTDIR\CD_Install\Albatros"
  File        "${SORGENTI}\${ALBATROS_SP_CLIPPER}"

  SetOutPath   "$INSTDIR\CD_Install\TpaCad"
  File         "${SORGENTI}\${TPACAD_SP}"

  SetOutPath   "$INSTDIR\CD_Install\Nesting"
  File         "${SORGENTI}\${NESTING}"

  SetOutPath   "$INSTDIR\CD_Install\Elaborator"
  File         "${SORGENTI}\${ELABORATOR}"
  ;File        "${SORGENTI}${ELABORATOR_SP}"

  SetOutPath   "$INSTDIR\CD_Install\Custom"
  File         "${SORGENTI}\${ALB_CUSTOM_1}"

  SetOutPath   "$INSTDIR\CD_Install\BuilderApp"
  File         "${SORGENTI}\${BUILDERAPP}"

  ;--------------------------------

  ; Lancio tutti gli applicativi uno per uno, in modo da installarli tutti

  ;WSCM
  SetOutPath   "$INSTDIR"
  IfFileExists "$INSTDIR\CD_Install\${WSCM}" 0 +2
    ExecWait '"$INSTDIR\CD_Install\${WSCM}" /S /Path="$INSTDIR" /Version=Clipper /Simul3d=0 /Alb31=1 /Editor=TpaCad' $0

  SetOutPath   "$INSTDIR"
  IfFileExists "$INSTDIR\CD_Install\${WSCM_SP}" 0 +2
    ExecWait '"$INSTDIR\CD_Install\${WSCM_SP}" Path="$INSTDIR" -s Overwrite=1' $0              ;Cartelle BIN - LNG - TMP
    ;Devo attendere la fine dell'installazione del SP
    Call WaitSPUpdater

  ;TpaCad Sp (silent - Il file deve gi� trovarsi nella directory di installazione)
  SetOutPath   "$INSTDIR"
  IfFileExists "$INSTDIR\CD_Install\${TPACAD_SP}" 0 +2
    ExecWait '"$INSTDIR\CD_Install\${TPACAD_SP}" /S' $0                                        ;Cartelle BIN - DXF - HELP - LNG - TPACADCFG, file sovrascitti = Bin\Tpaspa.SplashAbaut.v2.dll

  ;Elaborator (silent - Il file deve gi� trovarsi nella directory di installazione)
  SetOutPath   "$INSTDIR"
  IfFileExists "$INSTDIR\CD_Install\${ELABORATOR}" 0 +2
    ExecWait '"$INSTDIR\CD_Install\${ELABORATOR}" /S' $0                                       ;Cartelle BIN, sovrascitti = MaacTecno.dll

;  ;Elaborator Sp (silent - Il file deve gi� trovarsi nella directory di installazione)
;  IfFileExists "$INSTDIR\CD_Install\${ELABORATOR_SP}" 0 +2
;    ExecWait '"$INSTDIR\CD_Install\${ELABORATOR_SP}" /S' $0

  ;BuilderApp (silent - Il file deve gi� trovarsi nella directory di installazione)
  SetOutPath   "$INSTDIR"
  IfFileExists "$INSTDIR\CD_Install\${BUILDERAPP}" 0 +2
    ExecWait '"$INSTDIR\CD_Install\${BUILDERAPP}" /S' $0                                       ;Cartelle BIN - LNG - TPACADCFG, nessuna sovrascittura

  ;Nesting, per ora lasciamolo perdere
  SetOutPath   "$INSTDIR"
  IfFileExists "$INSTDIR\CD_Install\${NESTING}" 0 +2
    ExecWait '"$INSTDIR\CD_Install\${NESTING}" /S /Path="$INSTDIR" /Type=APP' $0               ;Cartelle BIN - LNG - HELP - SYSTEM, file sovrascitti = Bin\AlnPiece.dll
                                                                                                                                                      ;Bin\Interop.ALBPIECEOPLib.dll
                                                                                                                                                      ;Bin\TpaSpa.CadGeos.v2.dll
                                                                                                                                                      ;Bin\TpaSpa.CadLibrary.v2.dll
                                                                                                                                                      ;Bin\TpaSpa.CadModel.v2.dll
                                                                                                                                                      ;Bin\TpaSpa.CadOpti.v2.dll
                                                                                                                                                      ;Bin\TpaSpa.CadPanelDraw.v2.dll
                                                                                                                                                      ;Bin\TpaSpa.CadPiece.v2.dll
                                                                                                                                                      ;Bin\TpaSpa.CadPreview.v2.dll
                                                                                                                                                      ;Bin\TpaSpa.CadTecno.v2.dll
                                                                                                                                                      ;Bin\TpaSpa.FieldSelector.v2.dll
                                                                                                                                                      ;Bin\TpaSpa.Share.v2.dll
                                                                                                                                                      ;Bin\TpaSpa.SplashAbout.v2.dll
                                                                                                                                                      ;Bin\TpaSpa.StandardOptim.v2.dll
                                                                                                                                                      ;Bin\TpaSpa.TpaMath.v2.dll


  ;Albatros Sp (silent - Il file deve gi� trovarsi nella directory di installazione)
  SetOutPath   "$INSTDIR"
  IfFileExists "$INSTDIR\CD_Install\${ALBATROS_SP_CLIPPER}" 0 +2
    ExecWait '"$INSTDIR\CD_Install\${ALBATROS_SP_CLIPPER}" Path="$INSTDIR" -s Overwrite=1' $0  ;Cartelle BIN - LNG - HELP - SYSTEM, file sovrascitti = Bin\TpaSpa.Language.v2.dll
                                                                                                                                                      ;Bin\TpaSpa.Share.v2.dll
                                                                                                                                                      ;Bin\TpaSpa.SplashAbout.v2.dll
                                                                                                                                                      ;Bin\TpaSpa.TpaIni.v2.dll
                                                                                                                                                      ;Bin\TpaSpa.TpaLanguage.v2.dll
                                                                                                                                                      
  ;Custom     (silent - Il file deve gi� trovarsi nella directory di installazione) gli attributi per il silent sono gi� presenti nell'archivio
  SetOutPath   "$INSTDIR"
  IfFileExists "$INSTDIR\CD_Install\${ALB_CUSTOM_1}" 0 +2
    ExecWait '"$INSTDIR\CD_Install\${ALB_CUSTOM_1}"' $0                                          ;Cartelle MOD0 - SYSTEM
    
  ;--------------------------------
  ; Inserirsco le chiavi di registro

  Call CommonRegistryKey

SectionEnd



;--------------------------------

; Assign descriptions to sections

!insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
  !insertmacro MUI_DESCRIPTION_TEXT ${First}  $(Snova_Mech) ;msg
  !insertmacro MUI_DESCRIPTION_TEXT ${Second} $(Snova_Analog)
  ;!insertmacro MUI_DESCRIPTION_TEXT ${Third} ......
!insertmacro MUI_FUNCTION_DESCRIPTION_END

;--------------------------------

; Functions

Function WaitSPUpdater
Check:
  ${nsProcess::FindProcess} "ServicePackUpdater.exe" $R0
  ${If} $R0 == 0
      Goto Check
  ${EndIf}
FunctionEnd

Function CommonRegistryKey

  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_APPLICATION_NAME}_Ver.${PRODUCT_VERSION}.${PRODUCT_REVISION}" "DisplayName" "${PRODUCT_APPLICATION_NAME}_Ver.${PRODUCT_VERSION}.${PRODUCT_REVISION}"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_APPLICATION_NAME}_Ver.${PRODUCT_VERSION}.${PRODUCT_REVISION}" "UninstallString" "$INSTDIR\Uninstall ${PRODUCT_APPLICATION_NAME}.exe"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_APPLICATION_NAME}_Ver.${PRODUCT_VERSION}.${PRODUCT_REVISION}" "Publisher" "Tpa Spa"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_APPLICATION_NAME}_Ver.${PRODUCT_VERSION}.${PRODUCT_REVISION}" "URLInfoAbout" "http://www.TpaSpa.it"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_APPLICATION_NAME}_Ver.${PRODUCT_VERSION}.${PRODUCT_REVISION}" "HelpLink" "http://www.TpaSpa.it"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_APPLICATION_NAME}_Ver.${PRODUCT_VERSION}.${PRODUCT_REVISION}" "URLUpdateInfo" "http://www.TpaSpa.it"
  WriteUninstaller "$INSTDIR\Uninstall ${PRODUCT_APPLICATION_NAME}.exe"

FunctionEnd

Function .onInit

  ;Radio button, da ripetersi anche nella .onSelChange
  !insertmacro StartRadioButtons $1
    !insertmacro RadioButton ${First}
    !insertmacro RadioButton ${Second}
    ;!insertmacro RadioButton ${Third} .....
  !insertmacro EndRadioButtons

  ; Inserisco come direttorio di default per l'installazione "C:\Albatros\"
  StrCpy $INSTDIR "C:\Albatros\"

  ; Abilita file di log
  LogSet on

  ; Eseguo la macro denominata MUI_LANGDLL_DISPLAY
  !insertmacro MUI_LANGDLL_DISPLAY

FunctionEnd

Function .onSelChange

  ;Radio button, da ripetersi anche nella .onInit
  !insertmacro StartRadioButtons $1
    !insertmacro RadioButton ${First}
    !insertmacro RadioButton ${Second}
    ;!insertmacro RadioButton ${Third} .....
  !insertmacro EndRadioButtons

FunctionEnd

;----------------------------------------
#### Uninstaller code ####

Function un.onInit
  !insertmacro MUI_LANGDLL_DISPLAY
  Call un.CheckApplicationsRunning

  MessageBox MB_ICONQUESTION|MB_YESNO|MB_DEFBUTTON2 $(MSG_SureToUninstall) IDYES +2
  Abort
FunctionEnd

;--------- Gestione verifica applicazioni aperte ----------------
!macro CheckAppRunningAndAbort APP_PROCESS_NAME
  StrCpy $0 "${APP_PROCESS_NAME}"
  DetailPrint "Searching for processes called '$0'"
  KillProc::FindProcesses
${If} $1 == "-1"
    DetailPrint "-> Error: Something went wrong :-("
    Abort
${Else}
    DetailPrint "-> Found $0 processes running"
    ${If} $0 > 0
      MessageBox MB_ICONSTOP|MB_OK "${APP_PROCESS_NAME} is currently running. $\n$\nClose it and retry installation"
      DetailPrint "Installation Aborted!"
      Abort
    ${EndIf}
    DetailPrint "No Application Instances Found"
${EndIf}
!macroend

Function un.CheckAppRunningAndKill
  StrCpy $0 $2
  DetailPrint "Searching for processes called '$0'"
  KillProc::FindProcesses
  StrCmp $1 "-1" wooops
  Goto applRunning

applRunning:
    DetailPrint "-> Found $0 processes running"
    StrCmp $0 "0" applNotRunning
    MessageBox MB_ICONQUESTION|MB_YESNO|MB_DEFBUTTON2 "$2 is currently running. $\n$\nDo you want to close it and continue installation?" IDYES killApp
    DetailPrint "Installation Aborted!"
    Abort
killApp:
    StrCpy $0 $2
    DetailPrint "Killing all processes called '$0'"
    KillProc::KillProcesses
    StrCmp $1 "-1" wooops
    DetailPrint "-> Killed $0 processes, failed to kill $1 processes"
    sleep 1500
    Goto applNotRunning
wooops:
    DetailPrint "-> Error: Something went wrong :-("
    Abort
applNotRunning:
    DetailPrint "No Application Instances Found"
FunctionEnd

Function un.CheckApplicationsRunning
  StrCpy $2 "StosEmul.exe"
  Call un.CheckAppRunningAndKill
  StrCpy $2 "StoneEMU.exe"
  Call un.CheckAppRunningAndKill
  ;!insertmacro CheckAppRunningAndKill "StosEmul.exe"
  ;!insertmacro CheckAppRunningAndKill "StoneEMU.exe"

  !insertmacro CheckAppRunningAndAbort "WSCF.exe"
  !insertmacro CheckAppRunningAndAbort "WSCM.exe"
  !insertmacro CheckAppRunningAndAbort "WSC.exe"
  !insertmacro CheckAppRunningAndAbort "CncBoard.exe"
;;  !insertmacro CheckAppRunningAndAbort "albatros.exe"
;;  !insertmacro CheckAppRunningAndAbort "tpapass.exe"
;;  !insertmacro CheckAppRunningAndAbort "winimage.exe"
;;  !insertmacro CheckAppRunningAndAbort "winmess.exe"
  !insertmacro CheckAppRunningAndAbort "ArcManag.exe"
  !insertmacro CheckAppRunningAndAbort "BarCode.exe"
  !insertmacro CheckAppRunningAndAbort "DBConfEdit.exe"
  !insertmacro CheckAppRunningAndAbort "DbEdi32.exe"
  !insertmacro CheckAppRunningAndAbort "DBWorkEdit.exe"
  !insertmacro CheckAppRunningAndAbort "Nesting.exe"
  !insertmacro CheckAppRunningAndAbort "Optimizer.exe"
  !insertmacro CheckAppRunningAndAbort "OutfArc.exe"
  !insertmacro CheckAppRunningAndAbort "PanEdit.exe"
  !insertmacro CheckAppRunningAndAbort "ParPlain.exe"
  !insertmacro CheckAppRunningAndAbort "Simul3D.exe"
  !insertmacro CheckAppRunningAndAbort "Simulator.exe"
  !insertmacro CheckAppRunningAndAbort "TechPar.exe"
  !insertmacro CheckAppRunningAndAbort "ToolsArc.exe"
  !insertmacro CheckAppRunningAndAbort "XmlngEdit.exe"
  !insertmacro CheckAppRunningAndAbort "YaskCom.exe"
  !insertmacro CheckAppRunningAndAbort "YaskCom2.exe"
  !insertmacro CheckAppRunningAndAbort "WscBenchMark.exe"
  !insertmacro CheckAppRunningAndAbort "LanguageConverter.exe"
  !insertmacro CheckAppRunningAndAbort "InvCom.exe"
  !insertmacro CheckAppRunningAndAbort "InvCom2.exe"
  !insertmacro CheckAppRunningAndAbort "TpaEdi32.exe"
  !insertmacro CheckAppRunningAndAbort "TpaCad.exe"
  !insertmacro CheckAppRunningAndAbort "StosEmul.exe"
  !insertmacro CheckAppRunningAndAbort "StoneEMU.exe"
  !insertmacro CheckAppRunningAndAbort "${PRODUCT_APPLICATION_NAME}.exe"

  !insertmacro CheckAppRunningAndAbort "Langs.exe"
  !insertmacro CheckAppRunningAndAbort "HOOKME.EXE"

FunctionEnd
;--------------------------------------------------------------------------------

Function un.onUninstSuccess
  HideWindow
  MessageBox MB_ICONINFORMATION|MB_OK $(MSG_ProductUninstalled)
FunctionEnd

Function un.CancellaSeDirVuota
  ${DirState} $DirToCheck $DirState
  ${If} $DirState = 0
    RMDir $DirToCheck
  ${EndIf}
FunctionEnd

Section Uninstall
  ReadRegStr $StartMenuFolder HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_APPLICATION_NAME}_Ver.${PRODUCT_VERSION}.${PRODUCT_REVISION}" "NSIS:StartMenuDir"

  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_APPLICATION_NAME}_Ver.${PRODUCT_VERSION}.${PRODUCT_REVISION}"
  DeleteRegValue HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_APPLICATION_NAME}_Ver.${PRODUCT_VERSION}.${PRODUCT_REVISION}" "NSIS:StartMenuDir"

  
  Delete "$INSTDIR\Uninstall ${PRODUCT_APPLICATION_NAME}.exe"

  ;Delete temporary installation Files
  Delete "$INSTDIR\CD_Install\${WSCM}"
  Delete "$INSTDIR\CD_Install\${WSCM_SP}"
  Delete "$INSTDIR\CD_Install\${ALBATROS_SP_CLIPPER}"
  Delete "$INSTDIR\CD_Install\${TPACAD_SP}"
  Delete "$INSTDIR\CD_Install\${NESTING}"
  ;Delete "$INSTDIR\CD_Install\${NESTING_SP}"
  ;Delete "$INSTDIR\CD_Install\${ELABORATOR_SP}"
  Delete "$INSTDIR\CD_Install\${ALB_CUSTOM_0}"
  Delete "$INSTDIR\CD_Install\${ALB_CUSTOM_1}"
  Delete "$INSTDIR\CD_Install\${ALB_CUSTOM_2}"
  Delete "$INSTDIR\CD_Install\${BUILDERAPP}"
    
  StrCpy $DirToCheck "$INSTDIR\CD_Install"
  RMDir $DirToCheck
  ;Call un.CancellaSeDirVuota

  SetAutoClose true

SectionEnd