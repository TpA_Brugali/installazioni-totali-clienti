;Descrizione License File
LicenseLangString LicenseFile ${LANG_ITALIAN} "Licenza software TPA - italiano.rtf"
LicenseLangString LicenseFile ${LANG_ENGLISH} "Licenza software TPA - inglese.rtf"
LicenseLangString LicenseFile ${LANG_FRENCH}  "Licenza software TPA - francese.rtf"
LicenseLangString LicenseFile ${LANG_GERMAN}  "Licenza software TPA - tedesco.rtf"
LicenseLangString LicenseFile ${LANG_SPANISH} "Licenza software TPA - spagnolo.rtf"

;Descrizione per DrawBox
LangString MSG_InstTypeDrawBox ${LANG_ITALIAN} "Installazione ${PRODUCT_NAME}"
LangString MSG_InstTypeDrawBox ${LANG_ENGLISH} "Setup for ${PRODUCT_NAME}"
LangString MSG_InstTypeDrawBox ${LANG_FRENCH}  "Installation pour ${PRODUCT_NAME}"
LangString MSG_InstTypeDrawBox ${LANG_GERMAN}  "Installation f�r ${PRODUCT_NAME}"
LangString MSG_InstTypeDrawBox ${LANG_SPANISH} "Instalaci�n por ${PRODUCT_NAME}"

;Descrizione per Albatros 3.1
LangString MSG_InstTypeAlbatros ${LANG_ITALIAN} "Installazione Albatros 3.1"
LangString MSG_InstTypeAlbatros ${LANG_ENGLISH} "Setup for Albatros 3.1"
LangString MSG_InstTypeAlbatros ${LANG_FRENCH}  "Installation pour Albatros 3.1"
LangString MSG_InstTypeAlbatros ${LANG_GERMAN}  "Installation f�r Albatros 3.1"
LangString MSG_InstTypeAlbatros ${LANG_SPANISH} "Instalaci�n por Albatros 3.1"

;Albatros � gi� installato nel PC: disinstallare prima di installare DrawBox
LangString MSG_AlbatrosAlreadyInstalled ${LANG_ITALIAN} "Albatros � gi� installato nel PC: disinstallare prima di installare DrawBox"
LangString MSG_AlbatrosAlreadyInstalled ${LANG_ENGLISH} "Albatros already installed: uninstall before proceeding"
LangString MSG_AlbatrosAlreadyInstalled ${LANG_FRENCH}  "Albatros already installed: uninstall before proceeding"
LangString MSG_AlbatrosAlreadyInstalled ${LANG_GERMAN}  "Albatros already installed: uninstall before proceeding"
LangString MSG_AlbatrosAlreadyInstalled ${LANG_SPANISH} "Albatros already installed: uninstall before proceeding"

;Descrizione per TpaCad
LangString MSG_InstTypeTpaCad ${LANG_ITALIAN} "Installazione TpaCad"
LangString MSG_InstTypeTpaCad ${LANG_ENGLISH} "Setup for TpaCad"
LangString MSG_InstTypeTpaCad ${LANG_FRENCH}  "Installation pour TpaCad"
LangString MSG_InstTypeTpaCad ${LANG_GERMAN}  "Installation f�r TpaCad"
LangString MSG_InstTypeTpaCad ${LANG_SPANISH} "Instalaci�n por TpaCad"

;TpaCad � gi� installato nel PC: disinstallare prima di installare DrawBox
LangString MSG_TpaCadAlreadyInstalled ${LANG_ITALIAN} "TpaCad � gi� installato nel PC: disinstallare prima di installare DrawBox"
LangString MSG_TpaCadAlreadyInstalled ${LANG_ENGLISH} "TpaCad already installed: uninstall before proceeding"
LangString MSG_TpaCadAlreadyInstalled ${LANG_FRENCH}  "TpaCad already installed: uninstall before proceeding"
LangString MSG_TpaCadAlreadyInstalled ${LANG_GERMAN}  "TpaCad already installed: uninstall before proceeding"
LangString MSG_TpaCadAlreadyInstalled ${LANG_SPANISH} "TpaCad already installed: uninstall before proceeding"

;Descrizione per Nesting
LangString MSG_InstTypeNesting ${LANG_ITALIAN} "Installazione Nesting"
LangString MSG_InstTypeNesting ${LANG_ENGLISH} "Setup for Nesting"
LangString MSG_InstTypeNesting ${LANG_FRENCH}  "Installation pour Nesting"
LangString MSG_InstTypeNesting ${LANG_GERMAN}  "Installation f�r Nesting"
LangString MSG_InstTypeNesting ${LANG_SPANISH} "Instalaci�n por Nesting"

;Nesting � gi� installato nel PC: disinstallare prima di installare DrawBox
LangString MSG_NestingAlreadyInstalled ${LANG_ITALIAN} "Nesting � gi� installato nel PC: disinstallare prima di installare DrawBox"
LangString MSG_NestingAlreadyInstalled ${LANG_ENGLISH} "Nesting already installed: uninstall before proceeding"
LangString MSG_NestingAlreadyInstalled ${LANG_FRENCH}  "Nesting already installed: uninstall before proceeding"
LangString MSG_NestingAlreadyInstalled ${LANG_GERMAN}  "Nesting already installed: uninstall before proceeding"
LangString MSG_NestingAlreadyInstalled ${LANG_SPANISH} "Nesting already installed: uninstall before proceeding"

;Descrizione per Proiettore
LangString MSG_InstTypeProjector ${LANG_ITALIAN} "Installazione opzione Proiettore"
LangString MSG_InstTypeProjector ${LANG_ENGLISH} "Setup for Projector option"
LangString MSG_InstTypeProjector ${LANG_FRENCH}  "Installation pour Projecteur"
LangString MSG_InstTypeProjector ${LANG_GERMAN}  "Installation f�r Projector"
LangString MSG_InstTypeProjector ${LANG_SPANISH} "Instalaci�n por Proyector"

;Descrizione sezione tipo macchina Snova - Mechatrolink
LangString Snova_Mech ${LANG_ITALIAN} "Macchina Snova-Mechatrolink"
LangString Snova_Mech ${LANG_ENGLISH} "Snova-Mechatrolink machine"
LangString Snova_Mech ${LANG_FRENCH}  "Snova-Mechatrolink machine"
LangString Snova_Mech ${LANG_GERMAN}  "Snova-Mechatrolink machine"
LangString Snova_Mech ${LANG_SPANISH} "Snova-Mechatrolink machine"

;Descrizione sezione tipo macchina Snova - Analogic
LangString Snova_Analog ${LANG_ITALIAN} "Macchina Snova_Analog"
LangString Snova_Analog ${LANG_ENGLISH} "Snova-Analogic machine"
LangString Snova_Analog ${LANG_FRENCH}  "Snova-Analogic machine"
LangString Snova_Analog ${LANG_GERMAN}  "Snova-Analogic machine"
LangString Snova_Analog ${LANG_SPANISH} "Snova-Analogic machine"

;Descrizione sezione tipo macchina Exitech E2
LangString E2Description ${LANG_ITALIAN} "E2. Centro di lavoro tre assi modello base."
LangString E2Description ${LANG_ENGLISH} "E2. Three axis working center basic model."
LangString E2Description ${LANG_FRENCH}  "E2. Three axis working center basic model."
LangString E2Description ${LANG_GERMAN}  "E2. Three axis working center basic model."
LangString E2Description ${LANG_SPANISH} "E2. Three axis working center basic model."

;Descrizione sezione tipo macchina Exitech E3
LangString E3Description ${LANG_ITALIAN} "E3. Centro di lavoro tre / quattro assi con testa a forare."
LangString E3Description ${LANG_ENGLISH} "E3. Three / four axis working center with drill box."
LangString E3Description ${LANG_FRENCH}  "E3. Three / four axis working center with drill box."
LangString E3Description ${LANG_GERMAN}  "E3. Three / four axis working center with drill box."
LangString E3Description ${LANG_SPANISH} "E3. Three / four axis working center with drill box."

;Descrizione sezione tipo macchina Exitech E4
LangString E4Description ${LANG_ITALIAN} "E4. Centro di lavoro tre / quattro assi con testa a forare e carico / scarico automatico."
LangString E4Description ${LANG_ENGLISH} "E4. Three / four axis working center with drill box and automatic load / unload."
LangString E4Description ${LANG_FRENCH}  "E4. Three / four axis working center with drill box and automatic load / unload."
LangString E4Description ${LANG_GERMAN}  "E4. Three / four axis working center with drill box and automatic load / unload."
LangString E4Description ${LANG_SPANISH} "E4. Three / four axis working center with drill box and automatic load / unload."

;Descrizione sezione tipo macchina Exitech E8
LangString E8Description ${LANG_ITALIAN} "E8. Centro di lavoro cinque / sei assi con tornio. Plancia ISO."
LangString E8Description ${LANG_ENGLISH} "E8. Five / six axis working center with lathe. Dashboard ISO."
LangString E8Description ${LANG_FRENCH}  "E8. Five / six axis working center with lathe. Dashboard ISO."
LangString E8Description ${LANG_GERMAN}  "E8. Five / six axis working center with lathe. Dashboard ISO."
LangString E8Description ${LANG_SPANISH} "E8. Five / six axis working center with lathe. Dashboard ISO."

;Descrizione sezione tipo macchina KDT 00
LangString Custom_00Description ${LANG_ITALIAN} "KN-3408. Small Cnc machine v.1.3.2"
LangString Custom_00Description ${LANG_ENGLISH} "KN-3408. Small Cnc machine v.1.3.2"
LangString Custom_00Description ${LANG_FRENCH}  "KN-3408. Small Cnc machine v.1.3.2"
LangString Custom_00Description ${LANG_GERMAN}  "KN-3408. Small Cnc machine v.1.3.2"
LangString Custom_00Description ${LANG_SPANISH} "KN-3408. Small Cnc machine v.1.3.2"

;Descrizione sezione tipo macchina KDT 01
LangString Custom_01Description ${LANG_ITALIAN} "KN-3712. Cnc machine v.1.3.2"
LangString Custom_01Description ${LANG_ENGLISH} "KN-3712. Cnc machine v.1.3.2"
LangString Custom_01Description ${LANG_FRENCH}  "KN-3712. Cnc machine v.1.3.2"
LangString Custom_01Description ${LANG_GERMAN}  "KN-3712. Cnc machine v.1.3.2"
LangString Custom_01Description ${LANG_SPANISH} "KN-3712. Cnc machine v.1.3.2"

;Descrizione sezione Tipo di motore per giostra cambio utensile
LangString Kinco ${LANG_ITALIAN} "Giostra cambio utensile gestita tramite motore Kinco e protocollo CANopen"
LangString Kinco ${LANG_ENGLISH} "Carousel tool changer managed by Kinco motor and CANopen protocol."
LangString Kinco ${LANG_FRENCH}  "Carousel tool changer managed by Kinco motor and CANopen protocol."
LangString Kinco ${LANG_GERMAN}  "Carousel tool changer managed by Kinco motor and CANopen protocol."
LangString Kinco ${LANG_SPANISH} "Carousel tool changer managed by Kinco motor and CANopen protocol."

;Descrizione sezione Labeling station
LangString Labeling ${LANG_ITALIAN} "Stazione etichettatura. Disponibile solo su macchine E3 o E4"
LangString Labeling ${LANG_ENGLISH} "Labeling station. Avaible only on E3 or E4 machines"
LangString Labeling ${LANG_FRENCH}  "Labeling station. Avaible only on E3 or E4 machines"
LangString Labeling ${LANG_GERMAN}  "Labeling station. Avaible only on E3 or E4 machines"
LangString Labeling ${LANG_SPANISH} "Labeling station. Avaible only on E3 or E4 machines"

;Descrizione sezione ISOCNC
LangString AlbatrosDescription ${LANG_ITALIAN} "ALBATROS. Controllo numerico TPA."
LangString AlbatrosDescription ${LANG_ENGLISH} "ISOCNC. The TPA numerical control."
LangString AlbatrosDescription ${LANG_FRENCH}  "ISOCNC. The TPA numerical control."
LangString AlbatrosDescription ${LANG_GERMAN}  "ISOCNC. The TPA numerical control."
LangString AlbatrosDescription ${LANG_SPANISH} "ISOCNC. The TPA numerical control."

;Descrizione sezione TpaCad
LangString TpaCadDescription ${LANG_ITALIAN} "TpaCad. CAD-CAM per la progettazione e realizzazione di lavorazioni con macchine utensili."
LangString TpaCadDescription ${LANG_ENGLISH} "TpaCad. The TPA CAD-CAD for the developing and production of workings."
LangString TpaCadDescription ${LANG_FRENCH}  "TpaCad. The TPA CAD-CAD for the developing and production of workings."
LangString TpaCadDescription ${LANG_GERMAN}  "TpaCad. The TPA CAD-CAD for the developing and production of workings."
LangString TpaCadDescription ${LANG_SPANISH} "TpaCad. The TPA CAD-CAD for the developing and production of workings."

;Descrizione sezione Nesting
LangString NestingDescription ${LANG_ITALIAN} "Nesting. Applicazione per l'ottimizzazione degli sfridi su grezzo."
LangString NestingDescription ${LANG_ENGLISH} "Nesting. The TPA application for hole-unworked part optimization."
LangString NestingDescription ${LANG_FRENCH}  "Nesting. The TPA application for hole-unworked part optimization."
LangString NestingDescription ${LANG_GERMAN}  "Nesting. The TPA application for hole-unworked part optimization."
LangString NestingDescription ${LANG_SPANISH} "Nesting. The TPA application for hole-unworked part optimization."

;Descrizione sezione Proiettore
LangString ProjectorDescription ${LANG_ITALIAN} "Opzione proiettore. Gestione inserimento e visualizzazione di lavorazioni/profili su banco di lavoro"
LangString ProjectorDescription ${LANG_ENGLISH} "Projector option. The TPA application for the management and the visualization of workings on workbench."
LangString ProjectorDescription ${LANG_FRENCH}  "Projector option. The TPA application for the management and the visualization of workings on workbench."
LangString ProjectorDescription ${LANG_GERMAN}  "Projector option. The TPA application for the management and the visualization of workings on workbench."
LangString ProjectorDescription ${LANG_SPANISH} "Projector option. The TPA application for the management and the visualization of workings on workbench."

;Fallita l'installazione del ServicePack
LangString MSG_FailedInstallServicePack ${LANG_ITALIAN} "Fallita installazione del Service Pack."
LangString MSG_FailedInstallServicePack ${LANG_ENGLISH} "Service Pack installation failed."
LangString MSG_FailedInstallServicePack ${LANG_FRENCH}  "Service Pack installation failed."
LangString MSG_FailedInstallServicePack ${LANG_GERMAN}  "Service Pack installation failed."
LangString MSG_FailedInstallServicePack ${LANG_SPANISH} "Service Pack installation failed."

;Installazione dell'ultimo ServicePack
LangString MSG_InstallLastServicePack ${LANG_ITALIAN} "Installazione dell'ultimo Service Pack dell'applicazione."
LangString MSG_InstallLastServicePack ${LANG_ENGLISH} "Latest application Service Pack installation."
LangString MSG_InstallLastServicePack ${LANG_FRENCH}  "Latest application Service Pack installation."
LangString MSG_InstallLastServicePack ${LANG_GERMAN}  "Latest application Service Pack installation."
LangString MSG_InstallLastServicePack ${LANG_SPANISH} "Latest application Service Pack installation."

;Testo ${PRODUCT_NAME} disinstallato
LangString MSG_ProductUninstalled ${LANG_ITALIAN} "${PRODUCT_NAME} � stato completamente rimosso dal tuo computer."
LangString MSG_ProductUninstalled ${LANG_ENGLISH} "${PRODUCT_NAME} was completely removed from your computer."
LangString MSG_ProductUninstalled ${LANG_FRENCH}  "${PRODUCT_NAME} a �t� compl�tement enlev� par ton ordinateur."
LangString MSG_ProductUninstalled ${LANG_GERMAN}  "${PRODUCT_NAME} von deinem Computer ganz entfernt."
LangString MSG_ProductUninstalled ${LANG_SPANISH} "${PRODUCT_NAME} ha sido completamente removido por tu ordenador."

;Testo conferma disinstallazione di ${PRODUCT_NAME}
LangString MSG_SureToUninstall ${LANG_ITALIAN} "Sei sicuro di voler completamente rimuovere ${PRODUCT_NAME} e tutti i suoi componenti?"
LangString MSG_SureToUninstall ${LANG_ENGLISH} "Are you sure to completely remove ${PRODUCT_NAME} and all its components?"
LangString MSG_SureToUninstall ${LANG_FRENCH}  "Tu es s�r de vouloir enlever ${PRODUCT_NAME} et tous ses composants?"
LangString MSG_SureToUninstall ${LANG_GERMAN}  "Du bist ganz ${PRODUCT_NAME} und alle seine Mitglieder entfernen wollen sicher?"
LangString MSG_SureToUninstall ${LANG_SPANISH} "Est�s seguro de querer remover completamente ${PRODUCT_NAME} y todos sus miembros?"
