
;2.0.0.0 -> 07/09/2016
;2.0.0.1 -> 08/09/2016 modificato custom installato

;----------------------------------------------------------------------------------------------
; Costanti
;----------------------------------------------------------------------------------------------

!define PRODUCT_APPLICATION_NAME "SNOVAR Machines CD"
!define WELCOME_NAME             "SNOVAR Machines"
!define PRODUCT_NAME             "MACHINE_CD"
!define FULL_PRODUCT_VERSION     "2.0.0.0"  ;Per pannello di controllo (vuole x.x.x.x)
!define PRODUCT_VERSION          "2.0"
!define PRODUCT_REVISION         "0"
!define PRODUCT_FOLDERNAME       "TPA"

;Installazioni di altri applicativi da aggiungere in modalit� silent
!define WSCM                     "WSCM\Setup WSCM 3.6.exe"
!define WSCM_SP                  "WSCM\ServicePack_3.6_22.exe"
!define ALBATROS_SP_CLIPPER      "Albatros\Service Pack 7 ver3.1 Clipper.exe"
!define TPACAD                   "TpaCad\Setup TpaCAD 1.4 + SP12.exe"
!define NESTING                  "Nesting\Nesting 3.7.11.0 Setup.exe"
;!define NESTING_SP               "Nesting\ServicePack_3.2_2.exe"


!define SORGENTI_PROGRAMS       "C:\Users\Niccol�\Desktop\Instal Tot\Programs\"
!define SORGENTI_BUILDS         "C:\Users\Niccol�\Desktop\Instal Tot\Builds\"
!define SORGENTI_CTERA          "\\ctera.tpaspa.it\Customers\SNOVAR\Installazione Totale"

!define ELABORATOR               "Elaborator\Setup Elaborator Ver1.3.exe"
!define ELABORATOR_SP            "Elaborator\Elaborator 1.3 Sp3.exe"

!define CT_ELABORATOR               "${SORGENTI_CTERA}\Elaborator\Setup Elaborator Ver1.3.exe"
!define CT_ELABORATOR_SP            "${SORGENTI_CTERA}\Elaborator\Elaborator 1.3 Sp3.exe"


!define BUILDERAPP               "BuilderApp\BuilderApp.exe"

!define CT_BUILDERAPP               "${SORGENTI_CTERA}\BuilderApp\BuilderApp.exe"

!define ALB_CUSTOM_0             "Custom\Machine4 - Nesting_foratori_lamata_array8posizioni.exe"
!define CT_ALB_CUSTOM_0             "${SORGENTI_CTERA}\Customs\Machine4 - Nesting_foratori_lamata_array8posizioni.exe"

!define MUI_ABORTWARNING
!define MUI_COMPONENTSPAGE_SMALLDESC

;----------------------------------------------------------------------------------------------
; Variabili
;----------------------------------------------------------------------------------------------

Var StartMenuFolder    ; Cartella di menu
Var DirToCheck         ; Directory da controllare se vuota
Var DirState           ; Contiene l'informazione se directory vuota

;----------------------------------------------------------------------------------------------
;----------------------------------------------------------------------------------------------
; Si parte con la configurazione del setup
;----------------------------------------------------------------------------------------------
;----------------------------------------------------------------------------------------------

; Decido l'algoritmo di compressione dell'eseguibile finale, da utilizzare sempre
; come prima istruzione
SetCompressor zlib

; Utilit� di base file e directory
!include FILEFUNC.NSH
; Utilit� di gestione sezioni
!include Sections.nsh
;Utilit� per gestione processi
!include nsProcess.nsh
!include logiclib.nsh

;----------------------------------------------------------------------------------------------
; User interface definition
;----------------------------------------------------------------------------------------------

; MUI 2.0 compatible
!include MUI2.nsh

;Icona
!define MUI_ICON   "${SORGENTI_PROGRAMS}\Icons\multiSizeGeneric.ico"
!define MUI_UNICON "${SORGENTI_PROGRAMS}\Icons\multiSizeGenericUnist.ico"

;replace the "Nullsoft Install System v2.45"
BrandingText "T.P.A. S.p.A. Install"

;Pagine visualizzate nell'installazione
!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_LICENSE $(LicenseFile)  ; License page

;!define MUI_STARTMENUPAGE_DEFAULTFOLDER "TPA"
;!define MUI_STARTMENUPAGE_REGISTRY_ROOT "HKLM"
;!define MUI_STARTMENUPAGE_REGISTRY_KEY "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_NAME}_${PRODUCT_VERSION}.${PRODUCT_REVISION}"
;!define MUI_STARTMENUPAGE_REGISTRY_VALUENAME "NSIS:StartMenuDir"
;!define MUI_STARTMENUPAGE_NODISABLE
;!insertmacro MUI_PAGE_STARTMENU Application $StartMenuFolder

; Component pages
;!insertmacro MUI_PAGE_COMPONENTS
; Directory page
!insertmacro MUI_PAGE_DIRECTORY
; Instfiles page
!insertmacro MUI_PAGE_INSTFILES
; Finish page
!insertmacro MUI_PAGE_FINISH

;Uninstall page
!insertmacro MUI_UNPAGE_INSTFILES

; Language files
!insertmacro MUI_LANGUAGE "Italian"
!insertmacro MUI_LANGUAGE "English"
!insertmacro MUI_LANGUAGE "French"
!insertmacro MUI_LANGUAGE "German"
!insertmacro MUI_LANGUAGE "Spanish"

;Messaggi in lingua
!include Messaggi.nsh

;----------------------------------------------------------------------------------------------
; Propriet�
;----------------------------------------------------------------------------------------------

Name                                  "${WELCOME_NAME}"
BrandingText                          "T.P.A. S.p.A. Install" ;replace the "Nullsoft Install System v2.45"
OutFile                               "${SORGENTI_BUILDS}\SNOVAR\Setup ${WELCOME_NAME} ${FULL_PRODUCT_VERSION}.exe"
InstallDir                            "C:\Albatros\"
ShowInstDetails                       show
ShowUnInstDetails                     show

;----------------------------------------------------------------------------------------------
; Informazioni di versione
;----------------------------------------------------------------------------------------------

VIProductVersion "${FULL_PRODUCT_VERSION}"
VIAddVersionKey /LANG=${LANG_ENGLISH} "ProductName" "${PRODUCT_APPLICATION_NAME}"
VIAddVersionKey /LANG=${LANG_ENGLISH} "Comments" ""
VIAddVersionKey /LANG=${LANG_ENGLISH} "CompanyName" "T.P.A. S.p.A."
VIAddVersionKey /LANG=${LANG_ENGLISH} "LegalTrademarks" ""
VIAddVersionKey /LANG=${LANG_ENGLISH} "LegalCopyright" "(c) 2014 TPA Spa. All rights reserved."
VIAddVersionKey /LANG=${LANG_ENGLISH} "FileDescription" "${PRODUCT_APPLICATION_NAME} Setup"
VIAddVersionKey /LANG=${LANG_ENGLISH} "ProductVersion" "${PRODUCT_VERSION}"
VIAddVersionKey /LANG=${LANG_ENGLISH} "FileVersion" "1.0"

;----------------------------------------------------------------------------------------------
;----------------------------------------------------------------------------------------------
; Sections
;----------------------------------------------------------------------------------------------
;----------------------------------------------------------------------------------------------

Section "Generics Install" Gen

  ;--------------------------------
  ; Copio i pacchetti d'installazione che mi servono

  CreateDirectory $INSTDIR
  CreateDirectory $INSTDIR\CD_Install
  CreateDirectory $INSTDIR\CD_Install\Albatros
  CreateDirectory $INSTDIR\CD_Install\TpaCad
  CreateDirectory $INSTDIR\CD_Install\Nesting
  CreateDirectory $INSTDIR\CD_Install\Elaborator
  CreateDirectory $INSTDIR\CD_Install\Custom
  CreateDirectory $INSTDIR\CD_Install\BuilderApp

  ;Applicazioni e Service Pack da installare - copiate in locale
  SetOverwrite ifnewer

  SetOutPath   "$INSTDIR\CD_Install\WSCM"
  File         "${SORGENTI_PROGRAMS}\${WSCM}"
  File         "${SORGENTI_PROGRAMS}\${WSCM_SP}"

  SetOutPath  "$INSTDIR\CD_Install\Albatros"
  File        "${SORGENTI_PROGRAMS}\${ALBATROS_SP_CLIPPER}"

  SetOutPath   "$INSTDIR\CD_Install\TpaCad"
  File         "${SORGENTI_PROGRAMS}\${TPACAD}"

  SetOutPath   "$INSTDIR\CD_Install\Nesting"
  File         "${SORGENTI_PROGRAMS}\${NESTING}"

  SetOutPath   "$INSTDIR\CD_Install\Elaborator"
  File         "${CT_ELABORATOR}"
  File         "${CT_ELABORATOR_SP}"

  SetOutPath   "$INSTDIR\CD_Install\Custom"
  File         "${CT_ALB_CUSTOM_0}"

  SetOutPath   "$INSTDIR\CD_Install\BuilderApp"
  File         "${CT_BUILDERAPP}"

  ;--------------------------------
  ; Lancio tutti gli applicativi uno per uno, in modo da installarli tutti

  ;WSCM
  SetOutPath   "$INSTDIR"
  IfFileExists "$INSTDIR\CD_Install\${WSCM}" 0 +2
    ExecWait '"$INSTDIR\CD_Install\${WSCM}" /S /Path="$INSTDIR" /Version=Clipper /Simul3d=0 /Alb31=1 /Editor=TpaCad' $0

  SetOutPath   "$INSTDIR"
  IfFileExists "$INSTDIR\CD_Install\${WSCM_SP}" 0 +2
    ExecWait '"$INSTDIR\CD_Install\${WSCM_SP}" Path="$INSTDIR" -s Overwrite=1' $0              
    ;Devo attendere la fine dell'installazione del SP
    Call WaitSPUpdater

  ;TpaCad Sp (silent - Il file deve gi� trovarsi nella directory di installazione)
  SetOutPath   "$INSTDIR"
  IfFileExists "$INSTDIR\CD_Install\${TPACAD}" 0 +2
    ExecWait '"$INSTDIR\CD_Install\${TPACAD}" /s /Path=$INSTDIR' $0                                            
    
  ;Albatros Sp (silent - Il file deve gi� trovarsi nella directory di installazione)
  SetOutPath   "$INSTDIR"
  IfFileExists "$INSTDIR\CD_Install\${ALBATROS_SP_CLIPPER}" 0 +2
    ExecWait '"$INSTDIR\CD_Install\${ALBATROS_SP_CLIPPER}" Path="$INSTDIR" -s Overwrite=1' $0 
                                                                                                                                                      
  ;Custom     (silent - Il file deve gi� trovarsi nella directory di installazione) gli attributi per il silent sono gi� presenti nell'archivio
  SetOutPath   "$INSTDIR"
  IfFileExists "$INSTDIR\CD_Install\${ALB_CUSTOM_0}" 0 +2
    ExecWait '"$INSTDIR\CD_Install\${ALB_CUSTOM_0}"' $0
                                                                                                 
  ;Elaborator (silent - Il file deve gi� trovarsi nella directory di installazione)
  SetOutPath   "$INSTDIR"
  IfFileExists "$INSTDIR\CD_Install\${ELABORATOR}" 0 +2
    ExecWait '"$INSTDIR\CD_Install\${ELABORATOR}" /S' $0                                       

  ;Elaborator Sp (silent - Il file deve gi� trovarsi nella directory di installazione)
  IfFileExists "$INSTDIR\CD_Install\${ELABORATOR_SP}" 0 +2
    ExecWait '"$INSTDIR\CD_Install\${ELABORATOR_SP}" /S' $0

  ;Nesting, per ora lasciamolo perdere
  SetOutPath   "$INSTDIR"
  IfFileExists "$INSTDIR\CD_Install\${NESTING}" 0 +2
    ExecWait '"$INSTDIR\CD_Install\${NESTING}" /S /Path="$INSTDIR" /Type=APP' $0               
    
  ;BuilderApp (silent - Il file deve gi� trovarsi nella directory di installazione)
  SetOutPath   "$INSTDIR"
  IfFileExists "$INSTDIR\CD_Install\${BUILDERAPP}" 0 +2
    ExecWait '"$INSTDIR\CD_Install\${BUILDERAPP}" /S' $0                                       
    
  ;--------------------------------
  ; Inserirsco le chiavi di registro

  Call Common

SectionEnd

;----------------------------------------------------------------------------------------------
; Assign descriptions to sections
;----------------------------------------------------------------------------------------------

;----------------------------------------------------------------------------------------------
;----------------------------------------------------------------------------------------------
;Sections End
;----------------------------------------------------------------------------------------------
;----------------------------------------------------------------------------------------------

;----------------------------------------------------------------------------------------------
; Functions
;----------------------------------------------------------------------------------------------

Function WaitSPUpdater
Check:
  ${nsProcess::FindProcess} "ServicePackUpdater.exe" $R0
  ${If} $R0 == 0
      Goto Check
  ${EndIf}
FunctionEnd

Function Common

  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_APPLICATION_NAME}_Ver.${PRODUCT_VERSION}.${PRODUCT_REVISION}" "DisplayName" "${PRODUCT_APPLICATION_NAME}_Ver.${PRODUCT_VERSION}.${PRODUCT_REVISION}"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_APPLICATION_NAME}_Ver.${PRODUCT_VERSION}.${PRODUCT_REVISION}" "UninstallString" "$INSTDIR\Uninstall ${PRODUCT_APPLICATION_NAME}.exe"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_APPLICATION_NAME}_Ver.${PRODUCT_VERSION}.${PRODUCT_REVISION}" "Publisher" "Tpa Spa"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_APPLICATION_NAME}_Ver.${PRODUCT_VERSION}.${PRODUCT_REVISION}" "URLInfoAbout" "http://www.TpaSpa.it"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_APPLICATION_NAME}_Ver.${PRODUCT_VERSION}.${PRODUCT_REVISION}" "HelpLink" "http://www.TpaSpa.it"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_APPLICATION_NAME}_Ver.${PRODUCT_VERSION}.${PRODUCT_REVISION}" "URLUpdateInfo" "http://www.TpaSpa.it"
  WriteUninstaller "$INSTDIR\Uninstall ${PRODUCT_APPLICATION_NAME}.exe"
  
  ;Vado ad inserire le lingue nel Tpa.ini, faccio una cosa comune a tutti i custom
  WriteINIStr "$INSTDIR\Bin\Tpa.ini" "WINMESS" "Languages" "SQI,BGR,ETI,CSY,DAN,NLD,FIN,ELL,HUN,LTH,LVI,PLK,PTG,ROM,RUS,SKY,SVE,TRK,SLV,JPN,CHS,HEB"
  WriteINIStr "$INSTDIR\Bin\Tpa.ini" "WINMESS" "DefaultLanguage" "ENG"
  WriteINIStr "$INSTDIR\Bin\Tpa.ini" "ALBATROS" "AlarmsHaveState" "1"
  WriteINIStr "$INSTDIR\Bin\Tpa.ini" "ALBATROS" "Debug.CompileBeforeSend" "1"
  WriteINIStr "$INSTDIR\Bin\Tpa.ini" "ALBATROS" "Edit.DefaultFont" "-12,0,0,0,400,0,0,0,0,3,2,1,49,Lucida Console"
  WriteINIStr "$INSTDIR\Bin\Tpa.ini" "ALBATROS" "Globals.DefaultFont" "-12,0,0,0,400,0,0,0,0,3,2,1,49,Lucida Console"
  WriteINIStr "$INSTDIR\Bin\Tpa.ini" "ALBATROS" "Param.DefaultFont" "-13,0,0,0,400,0,0,0,0,3,2,1,49,Lucida Console"
  WriteINIStr "$INSTDIR\Bin\Tpa.ini" "CNCBOARD" "ToolByPosition" "1"
  
  DeleteINIStr "$INSTDIR\Bin\Tpa.ini" "CNCBOARD" "CustomExe1"
  DeleteINIStr "$INSTDIR\Bin\Tpa.ini" "CNCBOARD" "CustomExe2"

FunctionEnd

Function .onInit

  ; Inserisco come direttorio di default per l'installazione "C:\Albatros\"
  StrCpy $INSTDIR "C:\Albatros\"

  ; Abilita file di log
  LogSet on

  ; Eseguo la macro denominata MUI_LANGDLL_DISPLAY
  !insertmacro MUI_LANGDLL_DISPLAY

FunctionEnd

;----------------------------------------------------------------------------------------------
;----------------------------------------------------------------------------------------------
#### Uninstaller code ####
;----------------------------------------------------------------------------------------------
;----------------------------------------------------------------------------------------------

Function un.onInit

  ; Inserisco come direttorio di default per l'installazione "C:\Albatros\"
  StrCpy $INSTDIR "C:\Albatros\"

  !insertmacro MUI_LANGDLL_DISPLAY
  Call un.CheckApplicationsRunning

  MessageBox MB_ICONQUESTION|MB_YESNO|MB_DEFBUTTON2 $(MSG_SureToUninstall) IDYES +2
  Abort
FunctionEnd

;--------- Gestione verifica applicazioni aperte ----------------
!macro CheckAppRunningAndAbort APP_PROCESS_NAME
  StrCpy $0 "${APP_PROCESS_NAME}"
  DetailPrint "Searching for processes called '$0'"
  KillProc::FindProcesses
${If} $1 == "-1"
    DetailPrint "-> Error: Something went wrong :-("
    Abort
${Else}
    DetailPrint "-> Found $0 processes running"
    ${If} $0 > 0
      MessageBox MB_ICONSTOP|MB_OK "${APP_PROCESS_NAME} is currently running. $\n$\nClose it and retry installation"
      DetailPrint "Installation Aborted!"
      Abort
    ${EndIf}
    DetailPrint "No Application Instances Found"
${EndIf}
!macroend

Function un.CheckAppRunningAndKill
  StrCpy $0 $2
  DetailPrint "Searching for processes called '$0'"
  KillProc::FindProcesses
  StrCmp $1 "-1" wooops
  Goto applRunning

applRunning:
    DetailPrint "-> Found $0 processes running"
    StrCmp $0 "0" applNotRunning
    MessageBox MB_ICONQUESTION|MB_YESNO|MB_DEFBUTTON2 "$2 is currently running. $\n$\nDo you want to close it and continue installation?" IDYES killApp
    DetailPrint "Installation Aborted!"
    Abort
killApp:
    StrCpy $0 $2
    DetailPrint "Killing all processes called '$0'"
    KillProc::KillProcesses
    StrCmp $1 "-1" wooops
    DetailPrint "-> Killed $0 processes, failed to kill $1 processes"
    sleep 1500
    Goto applNotRunning
wooops:
    DetailPrint "-> Error: Something went wrong :-("
    Abort
applNotRunning:
    DetailPrint "No Application Instances Found"
FunctionEnd

Function un.CheckApplicationsRunning
  StrCpy $2 "StosEmul.exe"
  Call un.CheckAppRunningAndKill
  StrCpy $2 "StoneEMU.exe"
  Call un.CheckAppRunningAndKill
  ;!insertmacro CheckAppRunningAndKill "StosEmul.exe"
  ;!insertmacro CheckAppRunningAndKill "StoneEMU.exe"

  !insertmacro CheckAppRunningAndAbort "WSCF.exe"
  !insertmacro CheckAppRunningAndAbort "WSCM.exe"
  !insertmacro CheckAppRunningAndAbort "WSC.exe"
  !insertmacro CheckAppRunningAndAbort "CncBoard.exe"
  !insertmacro CheckAppRunningAndAbort "albatros.exe"
  !insertmacro CheckAppRunningAndAbort "tpapass.exe"
;;  !insertmacro CheckAppRunningAndAbort "winimage.exe"
;;  !insertmacro CheckAppRunningAndAbort "winmess.exe"
  !insertmacro CheckAppRunningAndAbort "ArcManag.exe"
  !insertmacro CheckAppRunningAndAbort "BarCode.exe"
  !insertmacro CheckAppRunningAndAbort "DBConfEdit.exe"
  !insertmacro CheckAppRunningAndAbort "DbEdi32.exe"
  !insertmacro CheckAppRunningAndAbort "DBWorkEdit.exe"
  !insertmacro CheckAppRunningAndAbort "Nesting.exe"
  !insertmacro CheckAppRunningAndAbort "Optimizer.exe"
  !insertmacro CheckAppRunningAndAbort "OutfArc.exe"
  !insertmacro CheckAppRunningAndAbort "PanEdit.exe"
  !insertmacro CheckAppRunningAndAbort "ParPlain.exe"
  !insertmacro CheckAppRunningAndAbort "Simul3D.exe"
  !insertmacro CheckAppRunningAndAbort "Simulator.exe"
  !insertmacro CheckAppRunningAndAbort "TechPar.exe"
  !insertmacro CheckAppRunningAndAbort "ToolsArc.exe"
  !insertmacro CheckAppRunningAndAbort "XmlngEdit.exe"
  !insertmacro CheckAppRunningAndAbort "YaskCom.exe"
  !insertmacro CheckAppRunningAndAbort "YaskCom2.exe"
  !insertmacro CheckAppRunningAndAbort "WscBenchMark.exe"
  !insertmacro CheckAppRunningAndAbort "LanguageConverter.exe"
  !insertmacro CheckAppRunningAndAbort "InvCom.exe"
  !insertmacro CheckAppRunningAndAbort "InvCom2.exe"
  !insertmacro CheckAppRunningAndAbort "TpaEdi32.exe"
  !insertmacro CheckAppRunningAndAbort "TpaCad.exe"
  !insertmacro CheckAppRunningAndAbort "StosEmul.exe"
  !insertmacro CheckAppRunningAndAbort "StoneEMU.exe"
  !insertmacro CheckAppRunningAndAbort "${PRODUCT_APPLICATION_NAME}.exe"

  !insertmacro CheckAppRunningAndAbort "Langs.exe"
  !insertmacro CheckAppRunningAndAbort "HOOKME.EXE"

FunctionEnd
;--------------------------------------------------------------------------------

Function un.onUninstSuccess
  HideWindow
  MessageBox MB_ICONINFORMATION|MB_OK $(MSG_ProductUninstalled)
FunctionEnd

Function un.CancellaSeDirVuota
  ${DirState} $DirToCheck $DirState
  ${If} $DirState = 0
    RMDir $DirToCheck
  ${EndIf}
FunctionEnd

Section Uninstall
  ReadRegStr $StartMenuFolder HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_APPLICATION_NAME}_Ver.${PRODUCT_VERSION}.${PRODUCT_REVISION}" "NSIS:StartMenuDir"

  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_APPLICATION_NAME}_Ver.${PRODUCT_VERSION}.${PRODUCT_REVISION}"
  DeleteRegValue HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_APPLICATION_NAME}_Ver.${PRODUCT_VERSION}.${PRODUCT_REVISION}" "NSIS:StartMenuDir"

  
  Delete "$INSTDIR\Uninstall ${PRODUCT_APPLICATION_NAME}.exe"

  ;Delete temporary installation Files
  Delete "$INSTDIR\CD_Install\${WSCM}"
  Delete "$INSTDIR\CD_Install\${WSCM_SP}"
  Delete "$INSTDIR\CD_Install\${ALBATROS_SP_CLIPPER}"
  Delete "$INSTDIR\CD_Install\${TPACAD}"
  Delete "$INSTDIR\CD_Install\${NESTING}"
  ;Delete "$INSTDIR\CD_Install\${NESTING_SP}"
  ;Delete "$INSTDIR\CD_Install\${ELABORATOR_SP}"
  Delete "$INSTDIR\CD_Install\${ALB_CUSTOM_0}"
  Delete "$INSTDIR\CD_Install\${BUILDERAPP}"
    
  StrCpy $DirToCheck "$INSTDIR\CD_Install"
  RMDir $DirToCheck
  ;Call un.CancellaSeDirVuota

  SetAutoClose true

SectionEnd