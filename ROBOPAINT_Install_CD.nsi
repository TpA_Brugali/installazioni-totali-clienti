
;--------------------------------

; Costanti

!define PRODUCT_APPLICATION_NAME "Spraying Machines CD"
!define WELCOME_NAME             "RoboPaint"
!define PRODUCT_NAME             "RoboPaint"
!define FULL_PRODUCT_VERSION     "1.0.0.0"  ;Per pannello di controllo (vuole x.x.x.x)
!define PRODUCT_VERSION          "1.0"
!define PRODUCT_REVISION         "1"
!define PRODUCT_FOLDERNAME       "TPA"

;Installazioni di altri applicativi da aggiungere in modalit� silent
!define ALBATROS                 "Albatros\Setup Albatros 3.1 + SP6c.exe"
!define ROBO_PAINT               "RoboPaint\RoboPaint_9.3.2.0.exe"

;------------------Parametrizzare in funzione del custom da installare------------------------

!define ALB_CUSTOM_0             "Custom\Custom_GODN_SCANNER.exe"

;----------------------------------------------------------------------------------------------

!define SORGENTI                 "C:\Users\Niccol�\Desktop\Sorgenti HD\WSC 4.0\Setup\Setup All_141215\RoboPaint"

!define MUI_ABORTWARNING
!define MUI_COMPONENTSPAGE_SMALLDESC

;--------------------------------

; Variabili

Var StartMenuFolder    ; Cartella di menu
Var DirToCheck         ; Directory da controllare se vuota
Var DirState           ; Contiene l'informazione se directory vuota

;-------------------------------------------------------------------------------------------

; Decido l'algoritmo di compressione dell'eseguibile finale, da utilizzare sempre
; come prima istruzione
SetCompressor zlib

; Utilit� di base file e directory
!include FILEFUNC.NSH
; Utilit� di gestione sezioni
!include Sections.nsh
;Utilit� per gestione processi
!include nsProcess.nsh

;--------------------------------

; User interface definition

; MUI 2.0 compatible
!include MUI2.nsh

;Icona
!define MUI_ICON   "${SORGENTI}\Icons\multiSizeGeneric.ico"
!define MUI_UNICON "${SORGENTI}\Icons\multiSizeGenericUnist.ico"

;replace the "Nullsoft Install System v2.45"
BrandingText "T.P.A. S.p.A. Install"

;Pagine visualizzate nell'installazione
!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_LICENSE $(LicenseFile)  ; License page

;!define MUI_STARTMENUPAGE_DEFAULTFOLDER "TPA"
;!define MUI_STARTMENUPAGE_REGISTRY_ROOT "HKLM"
;!define MUI_STARTMENUPAGE_REGISTRY_KEY "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_NAME}_${PRODUCT_VERSION}.${PRODUCT_REVISION}"
;!define MUI_STARTMENUPAGE_REGISTRY_VALUENAME "NSIS:StartMenuDir"
;!define MUI_STARTMENUPAGE_NODISABLE
;!insertmacro MUI_PAGE_STARTMENU Application $StartMenuFolder

; Directory page
!insertmacro MUI_PAGE_DIRECTORY
; Instfiles page
!insertmacro MUI_PAGE_INSTFILES
; Finish page
!insertmacro MUI_PAGE_FINISH

;Uninstall page
!insertmacro MUI_UNPAGE_INSTFILES

; Language files
!insertmacro MUI_LANGUAGE "Italian"
!insertmacro MUI_LANGUAGE "English"
!insertmacro MUI_LANGUAGE "French"
!insertmacro MUI_LANGUAGE "German"
!insertmacro MUI_LANGUAGE "Spanish"

;Messaggi in lingua
!include Messaggi.nsh

;--------------------------------

; Propriet�

Name                                  "${WELCOME_NAME}"
BrandingText                          "T.P.A. S.p.A. Install" ;replace the "Nullsoft Install System v2.45"
OutFile                               "${SORGENTI}\Setup ${WELCOME_NAME} ${FULL_PRODUCT_VERSION}.exe"
InstallDir                            "C:\Albatros\"
ShowInstDetails                       show
ShowUnInstDetails                     show

;--------------------------------

; Informazioni di versione

VIProductVersion "${FULL_PRODUCT_VERSION}"
VIAddVersionKey /LANG=${LANG_ENGLISH} "ProductName" "${PRODUCT_APPLICATION_NAME}"
VIAddVersionKey /LANG=${LANG_ENGLISH} "Comments" ""
VIAddVersionKey /LANG=${LANG_ENGLISH} "CompanyName" "T.P.A. S.p.A."
VIAddVersionKey /LANG=${LANG_ENGLISH} "LegalTrademarks" ""
VIAddVersionKey /LANG=${LANG_ENGLISH} "LegalCopyright" "(c) 2014 TPA Spa. All rights reserved."
VIAddVersionKey /LANG=${LANG_ENGLISH} "FileDescription" "${PRODUCT_APPLICATION_NAME} Setup"
VIAddVersionKey /LANG=${LANG_ENGLISH} "ProductVersion" "${PRODUCT_VERSION}"
VIAddVersionKey /LANG=${LANG_ENGLISH} "FileVersion" "1.0"

;--------------------------------

; Sections

Section "Generics Install" Gen

  ;--------------------------------

  ; Copio i pacchetti d'installazione che mi servono

  CreateDirectory $INSTDIR
  CreateDirectory $INSTDIR\CD_Install
  CreateDirectory $INSTDIR\CD_Install\Albatros
  CreateDirectory $INSTDIR\CD_Install\RoboPaint
  CreateDirectory $INSTDIR\CD_Install\Custom

  ;Applicazioni e Service Pack da installare - copiate in locale
  SetOverwrite ifnewer

  SetOutPath  "$INSTDIR\CD_Install\Albatros"
  File        "${SORGENTI}\${ALBATROS}"

  SetOutPath   "$INSTDIR\CD_Install\RoboPaint"
  File         "${SORGENTI}\${ROBO_PAINT}"

  SetOutPath   "$INSTDIR\CD_Install\Custom"
  File         "${SORGENTI}\${ALB_CUSTOM_0}"

  ;--------------------------------

  ; Lancio tutti gli applicativi uno per uno, in modo da installarli tutti

  ;Albatros (Silent)
  SetOutPath   "$INSTDIR"
  IfFileExists "$INSTDIR\CD_Install\${ALBATROS}" 0 +2
    ExecWait '"$INSTDIR\CD_Install\${ALBATROS}" /S /Path=$INSTDIR /Version=Clipper /Tools=1 /Maps=1' $0
    
  ;RoboPaint  gli attributi per il silent sono gi� presenti nell'archivio
  SetOutPath   "$INSTDIR"
  IfFileExists "$INSTDIR\CD_Install\${ROBO_PAINT}" 0 +2
    ExecWait '"$INSTDIR\CD_Install\${ROBO_PAINT}"' $0
    
  ;Custom     gli attributi per il silent sono gi� presenti nell'archivio
  SetOutPath   "$INSTDIR"
  IfFileExists "$INSTDIR\CD_Install\${ALB_CUSTOM_0}" 0 +2
    ExecWait '"$INSTDIR\CD_Install\${ALB_CUSTOM_0}"' $0
    
  ;--------------------------------
  ; Inserirsco le chiavi di registro

  Call Common

SectionEnd

Function Common

  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_APPLICATION_NAME}_Ver.${PRODUCT_VERSION}.${PRODUCT_REVISION}" "DisplayName" "${PRODUCT_APPLICATION_NAME}_Ver.${PRODUCT_VERSION}.${PRODUCT_REVISION}"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_APPLICATION_NAME}_Ver.${PRODUCT_VERSION}.${PRODUCT_REVISION}" "UninstallString" "$INSTDIR\Uninstall ${PRODUCT_APPLICATION_NAME}.exe"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_APPLICATION_NAME}_Ver.${PRODUCT_VERSION}.${PRODUCT_REVISION}" "Publisher" "Tpa Spa"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_APPLICATION_NAME}_Ver.${PRODUCT_VERSION}.${PRODUCT_REVISION}" "URLInfoAbout" "http://www.TpaSpa.it"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_APPLICATION_NAME}_Ver.${PRODUCT_VERSION}.${PRODUCT_REVISION}" "HelpLink" "http://www.TpaSpa.it"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_APPLICATION_NAME}_Ver.${PRODUCT_VERSION}.${PRODUCT_REVISION}" "URLUpdateInfo" "http://www.TpaSpa.it"
  WriteUninstaller "$INSTDIR\Uninstall ${PRODUCT_APPLICATION_NAME}.exe"
  
  ;Vado ad inserire le lingue nel Tpa.ini, faccio una cosa comune a tutti i custom
  WriteINIStr "$INSTDIR\Bin\Tpa.ini" "TPA" "Albatros32" "1"
  WriteINIStr "$INSTDIR\Bin\Tpa.ini" "TPA" "LngToXmlng" "1"
  WriteINIStr "$INSTDIR\Bin\Tpa.ini" "TPA" "HwClipper" "1"
  WriteINIStr "$INSTDIR\Bin\Tpa.ini" "ALBATROS" "AlarmsHaveState" "1"
  WriteINIStr "$INSTDIR\Bin\Tpa.ini" "cncboard" "TouchScreenMode" "1"

FunctionEnd

Function .onInit

  ; Inserisco come direttorio di default per l'installazione "C:\Albatros\"
  StrCpy $INSTDIR "C:\Albatros\"

  ; Abilita file di log
  LogSet on

  ; Eseguo la macro denominata MUI_LANGDLL_DISPLAY
  !insertmacro MUI_LANGDLL_DISPLAY

FunctionEnd

;----------------------------------------
#### Uninstaller code ####

Function un.onInit
  !insertmacro MUI_LANGDLL_DISPLAY
  Call un.CheckApplicationsRunning

  MessageBox MB_ICONQUESTION|MB_YESNO|MB_DEFBUTTON2 $(MSG_SureToUninstall) IDYES +2
  Abort
FunctionEnd

;--------- Gestione verifica applicazioni aperte ----------------
!macro CheckAppRunningAndAbort APP_PROCESS_NAME
  StrCpy $0 "${APP_PROCESS_NAME}"
  DetailPrint "Searching for processes called '$0'"
  KillProc::FindProcesses
${If} $1 == "-1"
    DetailPrint "-> Error: Something went wrong :-("
    Abort
${Else}
    DetailPrint "-> Found $0 processes running"
    ${If} $0 > 0
      MessageBox MB_ICONSTOP|MB_OK "${APP_PROCESS_NAME} is currently running. $\n$\nClose it and retry installation"
      DetailPrint "Installation Aborted!"
      Abort
    ${EndIf}
    DetailPrint "No Application Instances Found"
${EndIf}
!macroend

Function un.CheckAppRunningAndKill
  StrCpy $0 $2
  DetailPrint "Searching for processes called '$0'"
  KillProc::FindProcesses
  StrCmp $1 "-1" wooops
  Goto applRunning

applRunning:
    DetailPrint "-> Found $0 processes running"
    StrCmp $0 "0" applNotRunning
    MessageBox MB_ICONQUESTION|MB_YESNO|MB_DEFBUTTON2 "$2 is currently running. $\n$\nDo you want to close it and continue installation?" IDYES killApp
    DetailPrint "Installation Aborted!"
    Abort
killApp:
    StrCpy $0 $2
    DetailPrint "Killing all processes called '$0'"
    KillProc::KillProcesses
    StrCmp $1 "-1" wooops
    DetailPrint "-> Killed $0 processes, failed to kill $1 processes"
    sleep 1500
    Goto applNotRunning
wooops:
    DetailPrint "-> Error: Something went wrong :-("
    Abort
applNotRunning:
    DetailPrint "No Application Instances Found"
FunctionEnd

Function un.CheckApplicationsRunning
  StrCpy $2 "StosEmul.exe"
  Call un.CheckAppRunningAndKill
  StrCpy $2 "StoneEMU.exe"
  Call un.CheckAppRunningAndKill
  ;!insertmacro CheckAppRunningAndKill "StosEmul.exe"
  ;!insertmacro CheckAppRunningAndKill "StoneEMU.exe"

  !insertmacro CheckAppRunningAndAbort "WSCF.exe"
  !insertmacro CheckAppRunningAndAbort "WSCM.exe"
  !insertmacro CheckAppRunningAndAbort "WSC.exe"
  !insertmacro CheckAppRunningAndAbort "CncBoard.exe"
;;  !insertmacro CheckAppRunningAndAbort "albatros.exe"
;;  !insertmacro CheckAppRunningAndAbort "tpapass.exe"
;;  !insertmacro CheckAppRunningAndAbort "winimage.exe"
;;  !insertmacro CheckAppRunningAndAbort "winmess.exe"
  !insertmacro CheckAppRunningAndAbort "ArcManag.exe"
  !insertmacro CheckAppRunningAndAbort "BarCode.exe"
  !insertmacro CheckAppRunningAndAbort "DBConfEdit.exe"
  !insertmacro CheckAppRunningAndAbort "DbEdi32.exe"
  !insertmacro CheckAppRunningAndAbort "DBWorkEdit.exe"
  !insertmacro CheckAppRunningAndAbort "Nesting.exe"
  !insertmacro CheckAppRunningAndAbort "Optimizer.exe"
  !insertmacro CheckAppRunningAndAbort "OutfArc.exe"
  !insertmacro CheckAppRunningAndAbort "PanEdit.exe"
  !insertmacro CheckAppRunningAndAbort "ParPlain.exe"
  !insertmacro CheckAppRunningAndAbort "Simul3D.exe"
  !insertmacro CheckAppRunningAndAbort "Simulator.exe"
  !insertmacro CheckAppRunningAndAbort "TechPar.exe"
  !insertmacro CheckAppRunningAndAbort "ToolsArc.exe"
  !insertmacro CheckAppRunningAndAbort "XmlngEdit.exe"
  !insertmacro CheckAppRunningAndAbort "YaskCom.exe"
  !insertmacro CheckAppRunningAndAbort "YaskCom2.exe"
  !insertmacro CheckAppRunningAndAbort "WscBenchMark.exe"
  !insertmacro CheckAppRunningAndAbort "LanguageConverter.exe"
  !insertmacro CheckAppRunningAndAbort "InvCom.exe"
  !insertmacro CheckAppRunningAndAbort "InvCom2.exe"
  !insertmacro CheckAppRunningAndAbort "TpaEdi32.exe"
  !insertmacro CheckAppRunningAndAbort "TpaCad.exe"
  !insertmacro CheckAppRunningAndAbort "StosEmul.exe"
  !insertmacro CheckAppRunningAndAbort "StoneEMU.exe"
  !insertmacro CheckAppRunningAndAbort "${PRODUCT_APPLICATION_NAME}.exe"
  !insertmacro CheckAppRunningAndAbort "Langs.exe"
  !insertmacro CheckAppRunningAndAbort "HOOKME.EXE"

FunctionEnd
;--------------------------------------------------------------------------------

Function un.onUninstSuccess
  HideWindow
  MessageBox MB_ICONINFORMATION|MB_OK $(MSG_ProductUninstalled)
FunctionEnd

Function un.CancellaSeDirVuota
  ${DirState} $DirToCheck $DirState
  ${If} $DirState = 0
    RMDir $DirToCheck
  ${EndIf}
FunctionEnd

Section Uninstall
  ReadRegStr $StartMenuFolder HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_APPLICATION_NAME}_Ver.${PRODUCT_VERSION}.${PRODUCT_REVISION}" "NSIS:StartMenuDir"

  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_APPLICATION_NAME}_Ver.${PRODUCT_VERSION}.${PRODUCT_REVISION}"
  DeleteRegValue HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_APPLICATION_NAME}_Ver.${PRODUCT_VERSION}.${PRODUCT_REVISION}" "NSIS:StartMenuDir"

  
  Delete "$INSTDIR\Uninstall ${PRODUCT_APPLICATION_NAME}.exe"

  ;Delete temporary installation Files
  Delete "$INSTDIR\CD_Install\${ALBATROS}"
  Delete "$INSTDIR\CD_Install\${ROBO_PAINT}"
    
  StrCpy $DirToCheck "$INSTDIR\CD_Install"
  RMDir $DirToCheck
  ;Call un.CancellaSeDirVuota

  SetAutoClose true

SectionEnd